var unit = require('@seeessence/unit');
var stream = require('./index.js');

var foward = function (object, callback) {
    callback(null, object);
};

var back = function (object, callback) {
    callback(null, object);
};

unit('text', 'back function').pipe(
    stream(foward, back, 'stream').pipe(
        unit('text', 'foward function')
    )
);
