var debug = require('@seeessence/debug');
var EventEmitter = require('events').EventEmitter;
var PassThrough = require('stream').PassThrough;

module.exports = function (forward, back, name) {
    
    var stream = new EventEmitter();
    
    if (forward instanceof Function ||  typeof forward === 'function') {
        
        forward = (function (forward) {
            return new PassThrough({
                objectMode: true,
                transform: function(object, encoding, callback) {
                    forward(object, callback.bind(null));
                }
            });
        })(forward);
    }
    
    if (back instanceof Function ||  typeof venous === 'function') {
        
        back = (function (back) {
            
            return new PassThrough({
                objectMode: true,
                transform: function(object, encoding, callback) {
                    back(object, callback.bind(null));
                }
            });
        })(back);
        
    }
    
    process.env.npm_package_config_debug && debug(forward, name, arguments.callee);
    process.env.npm_package_config_debug && debug(back, name, arguments.callee);
    
    stream.pipe = function () {
        for (var argument in arguments)
            if (arguments.hasOwnProperty(argument))
                forward.pipe(arguments[argument], {end: false});
                
        return stream;
    };
    
    var emit = true;
    
    forward.on('pipe', function (source) {
        emit && source.unpipe(this);
        emit && source.pipe(back);
        emit = true;
    });
    
    stream.on('pipe', function (source) {
        source.unpipe(this);
        emit = false;
        source.pipe(forward);
        back.pipe(source);
    });
    
    return stream;
};
